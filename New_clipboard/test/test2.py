# -*- coding: UTF-8 -*-
import clipboard
import Image 
import binascii

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)


    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        text = clipboard.gettext()
        #print text
        assert(text == "hello, world!")
        assert(clipboard.getblob() == None)

    def test_copy_blob():
        clipboard.reset()
        f = open("test.jpg")
        img = f.read()
        clipboard.copyblob(img)
        blob = clipboard.getblob()
        #cv2.imshow('image',blob)
        #assert(clipboard.gettext() == None)
        assert(blob == img)

    def test_copy_text_after_text():
        clipboard.reset()
        clipboard.copytext("hello, world!")
        clipboard.copytext("some new text")
        text = clipboard.gettext()
        assert(text == "some new text")
        assert(clipboard.getblob() == None)
    
    def test_copy_blob_after_text():
        clipboard.reset()
        clipboard.copytext("some text")
        f = open("test.jpg")
        img = f.read()
        clipboard.copyblob(img)
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "some text")
        assert(blob == img)

    def test_copy_text_after_blob():
        clipboard.reset()
        f = open("test.jpg")
        img = f.read()
        clipboard.copyblob(img)
        clipboard.copytext("some text before blob")
        blob = clipboard.getblob()
        text = clipboard.gettext()
        assert(blob == img)
        assert(text == "some text before blob")
    
    def test_copy_blob_after_blob():
        clipboard.reset()
        f = open("test.jpg")
        img1 = f.read()
        g = open("test.jpg")
        img2 = f.read()
        clipboard.copyblob(img1)
        clipboard.copyblob(img2)
        blob = clipboard.getblob()
        #assert(clipboard.gettext() == None)
        assert(blob == img2)

    def test_copy_blob_after_loop_text():
        clipboard.reset()
        clipboard.copytext("some text")
        clipboard.copytext("next text")
        clipboard.copytext("third loop")
        f = open("test.jpg")
        img = f.read()
        clipboard.copyblob(img)
        text = clipboard.gettext()
        blob = clipboard.getblob()
        assert(text == "third loop")
        assert(blob == img)


    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    test_copy_blob()
    test_copy_text_after_text()
    test_copy_blob_after_text()
    test_copy_text_after_blob()
    test_copy_blob_after_blob()
    test_copy_blob_after_loop_text()


run_clipboard_tests()

"""
def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()   """

import sys
import pickle
import re
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET
__all__ = ["new_note", "delte_note_by_title", ]

__notes__ = []
__title__ = None
__body__ = None
#__titlelist__ = None

def new_note(title, body):
    global __notes__
    note = (title,body)
    __notes__.append(note)

def delete_note_by_title(title):
    #tit = []
    #pattern = '^w+'	
    #tit = re.findall(pattern, title)
    global __notes__
    for x in range(0,len(__notes__)-1):
        if re.search(title, __notes__[x][0]):
            __notes__.remove(__notes__[x])

def find_notes_by_title(title):
    global __notes__
    titlelist = []
    for x in range(len(__notes__)):
        if re.search(title, __notes__[x][0]):
            titlelist = __notes__[x]
            return titlelist 

def find_notes_by_key_in_body(key):
    global __notes__
    for y in __notes__:
        if re.search(key,y[1],re.I):
            print y[0]
            print y[1]
    
def getnotes():
    global __notes__
    return __notes__

def save_note_pickle():
    global __notes__
    file_Name = "testfile"
    fileObject = open(file_Name,"wb") 
    pickle.dump(__notes__,fileObject)   
    fileObject.close()
    return    

def save_note_xml():
    global __notes__
    root = ET.Element("__Notes__")
    for x in __notes__:
        doc = ET.SubElement(root, "Note")
        field1 = ET.SubElement(doc, "Title")
        field1.text = x[0]
        field2 = ET.SubElement(doc, "Body")
        field2.text = x[1]
    tree = ET.ElementTree(root)
    tree.write("Notes.xml")    



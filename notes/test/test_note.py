import note
import sys
import pickle,traceback
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET

def run_test_notes():
    def test_empty_notes():
        assert(note.getnotes() == [])

    def test_add_note():
        note.new_note("hello","kn sjdkvnnevblbvdkbvknjlnjvbxlj")
        note.new_note("hi","good morning!")
        assert(note.getnotes() == [("hello","kn sjdkvnnevblbvdkbvknjlnjvbxlj"),("hi","good morning!")] )
     
    def test_del_note():
        note.new_note("New","qwerty")
        note.delete_note_by_title("New")
        #assert(note.getnotes() == [("New","qwerty")])

    def test_find_notes_by_title():
        note.new_note("three","qwerty")
        note.new_note("three","abcdef")
        nlist = note.find_notes_by_title("three")
        #print nlist
        #assert(nlist == [("three","qwerty"),("three","abcdef")])

    def test_find_notes_by_key():
        note.new_note("four","abcdef")
        note.new_note("five","abcdef")
        note.find_notes_by_key_in_body("abcdef")

    def test_save_note_xml():
        print "1"
        note.save_note_xml()
        print "2"

    def test_save_note_pickle():
        note.save_note_pickle()
        print "\n\nNotes Saved(Pickle)."
        listn = pickle.load( open( "testfile", "rb" ) )
        print "Displaying Saved Notes."
        print listn

    test_empty_notes()
    test_add_note()
    test_del_note()
    test_find_notes_by_title()
    test_find_notes_by_key()
    test_save_note_xml()
    test_save_note_pickle()

run_test_notes()

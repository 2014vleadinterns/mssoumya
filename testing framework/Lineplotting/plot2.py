import matplotlib.pyplot as plt
import numpy as np
#import matplotlib.dates as mdates	
import sys
import csv    # it is a library 

intermediate_fname = sys.argv[1] #obtaining the filename from the experiment.sh
urlname = sys.argv[2]
def graph(intermediate_fname):
    s = []           # an empty list to hold the x values.
    q = []           # an empty list to hold the y values.
    h = 1            # just a flag value for controlling the graph plot.
    #print urlname
    with open(intermediate_fname, 'rb') as csvfile:
        read = csv.reader(csvfile, delimiter=',', quotechar='\n')
        fig = plt.figure()
        ax1 = fig.add_subplot(1,1,1, axisbg='white')
        #print "hi"
        for row in read:
            #print row[2]
            s.append(float(row[1]))
            q.append(float(row[2]))
            if h%5 == 0 and h != 0:
                plt.plot(s,q,'^-',label="n="+row[0])
                legend = plt.legend(loc='best', shadow=True)
                s=[]
                q=[]
            h +=1  
        #plt.clf()
        plt.title('concurrency vs no. of requests')
        #ax1 = fig.add_subplot(1,1,1, axisbg='white')
        plt.xlabel('Concurrency')
        plt.ylabel('Requests per second')
        #print "hello"
        fig.savefig(urlname+'.jpg')
        #plt.show()
        #plt.close()
graph(intermediate_fname)

 
#----------------------------------------------------------------------------------
"""def graph():
    date, value = np.loadtxt('sampleCSV.csv', delimiter=',', unpack=True, converters = {0: mdates.strpdate2num('%Y-%m-%d')})
    fig = plt.figure()
    ax1 = fig.add_subplot(1,1,1, axisbg='white')
    plt.plot_date(x=date, y=value, fmt='-')
    plt.title('title')
    plt.ylabel('value')
    plt.xlabel('date')
    plt.show()

graph()"""
    
         
    #for x in fp.readlines():
    #    l = x.split(",")
        #print l[0],l[2]
        #s.append(l[1])
        #q.append(l[2])
        #print s,"--",q
    #print s,"--",q
    #plt.title('concurrency vs no. of requests')
    #ax1 = fig.add_subplot(1,1,1, axisbg='white')
    #plt.xlabel('Concurrency')
    #plt.ylabel('Requests per second')
    #fig.savefig(filename+'.png')
    #plt.show()
    
""" n1, c1 = np.loadtxt('250.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n2, c2 = np.loadtxt('500.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n3, c3 = np.loadtxt('1000.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n4, c4 = np.loadtxt('2000.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    fig = plt.figure()
    print n1,c1
    print n2,c2
    ax1 = fig.add_subplot(1,1,1, axisbg='white')
    #plt.axis([0, 4000, 0, 4000])
    plt.plot(n1, c1, 'o-', label="n=250")
    plt.plot(n2, c2, 'o-', label="n=500")
    plt.plot(n3, c3, 'o-', label="n=1000")
    plt.plot(n4, c4, 'o-', label="n=2000")
    plt.title('concurrency vs no. of requests')
    legend = plt.legend(loc='best', shadow=True)
    #plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('Concurrency')
    plt.ylabel('Requests per second')
    fig.savefig('all_with_legend.png')
    plt.show()"""



#csvcut -c 2 /home/ms/testingframe/yslowNpagspeedGraphs/stats_pagespeed_on.csv | sed "1d"> prac1.csv
FILENAME=$1
count=0

while read LINE
do    
      count=$(( $count + 1 ))
      if [ "$count" -ge 2 ]
      then 
          csvcut -c $count /home/ms/testingframe/yslowNpagspeedGraphs/stats-2.csv | sed "1d" > prac1.csv
          python wo.py prac1.csv "$LINE"
          echo "$count $LINE"
      fi
done < $FILENAME

echo "\nTotal $count Lines read"

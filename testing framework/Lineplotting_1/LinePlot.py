import matplotlib.pyplot as plt
import numpy as np	
import sys
import csv    # it is a library for operating on csv files.
import os
path = '/home/ms/testingframe/images'
intermediate_fname = sys.argv[1] #obtaining the filename from the experiment.sh
urlname = sys.argv[2]            #obtaining the name of the url for which the graph is being plotted from experiment.sh
ylabel = sys.argv[3]
imgname = urlname+ylabel+'.jpg'
fullpath = os.path.join(path, imgname)
def graph(intermediate_fname):
    x = []           # an empty list to hold the x values.
    y = []           # an empty list to hold the y values.
    h = 1            # just a flag value for controlling the loop.
    #print urlname
    with open(intermediate_fname, 'rb') as csvfile:
        lines = csv.reader(csvfile, delimiter=',', quotechar='\n') # Return a reader object which will iterate over lines in the given 									     csvfile. 
        fig = plt.figure()      # The figure module provides the Figure, which contains all the plot elements. 
        ax1 = fig.add_subplot(1,1,1, axisbg='white')  # add_subplot adds a plot with the specified number of plots in the figure.
        for row in lines: # for iterating over the rows one by one.
            #print row[2]
            x.append(float(row[1])) # accessing the individual elements of a row by using row[1] or row [0] etc.
            y.append(float(row[2])) # adding these individual values into the respective list for x and y values.
            if h%5 == 0 and h != 0: # condition for plotting exactly 5 values in the line graph.
                plt.plot(x,y,'^-',label="n="+row[0]) # plt.plot() function is used for plotting the graph using the x and y   values. Here we pass the list of x and y values to plot a continuous line graph. This'^-' is used to specify which way the point on the line graph should be displayed. This form used, displays the point in a triangular shape. The label parameter is used to set the label for the legend.
                legend = plt.legend(loc='best', shadow=True) # plt.legend() is used for adding the legend to the graph for each line graph 								       in the figure.
                x=[]  # Making the x values list empty to plot another line graph.
                y=[]  # Making the y values list empty to plot another line graph.
            h +=1     # incrementing the value of the loop control variable.
        plt.title('concurrency vs '+ylabel) # Setting the title for the plot.
        plt.xlabel('Concurrency')  # Setting the label for the x axis.
        plt.ylabel(ylabel)  # Setting the label for the y axis.
        fig.savefig(fullpath)  # fig.savefig() is used to save the figure plotted.
        #plt.show() -- This statement is used to show the plot on execution but here it has been commented so as to have a complete       		               execution of the program without displaying any of the images one by one.
        #plt.clf()        
        #plt.close()
graph(intermediate_fname)

 
#----------------------------------------------------------------------------------
"""def graph():
    date, value = np.loadtxt('sampleCSV.csv', delimiter=',', unpack=True, converters = {0: mdates.strpdate2num('%Y-%m-%d')})
    fig = plt.figure()
    ax1 = fig.add_subplot(1,1,1, axisbg='white')
    plt.plot_date(x=date, y=value, fmt='-')
    plt.title('title')
    plt.ylabel('value')
    plt.xlabel('date')
    plt.show()

graph()"""
    
         
    #for x in fp.readlines():
    #    l = x.split(",")
        #print l[0],l[2]
        #s.append(l[1])
        #q.append(l[2])
        #print s,"--",q
    #print s,"--",q
    #plt.title('concurrency vs no. of requests')
    #ax1 = fig.add_subplot(1,1,1, axisbg='white')
    #plt.xlabel('Concurrency')
    #plt.ylabel('Requests per second')
    #fig.savefig(filename+'.png')
    #plt.show()
    
""" n1, c1 = np.loadtxt('250.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n2, c2 = np.loadtxt('500.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n3, c3 = np.loadtxt('1000.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    n4, c4 = np.loadtxt('2000.csv', delimiter=',', usecols=(0,1),dtype= float, unpack=True)
    fig = plt.figure()
    print n1,c1
    print n2,c2
    ax1 = fig.add_subplot(1,1,1, axisbg='white')
    #plt.axis([0, 4000, 0, 4000])
    plt.plot(n1, c1, 'o-', label="n=250")
    plt.plot(n2, c2, 'o-', label="n=500")
    plt.plot(n3, c3, 'o-', label="n=1000")
    plt.plot(n4, c4, 'o-', label="n=2000")
    plt.title('concurrency vs no. of requests')
    legend = plt.legend(loc='best', shadow=True)
    #plt.setp(lines, color='r', linewidth=2.0)
    plt.xlabel('Concurrency')
    plt.ylabel('Requests per second')
    fig.savefig('all_with_legend.png')
    plt.show()"""



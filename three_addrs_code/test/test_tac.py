import TAC
import sys 

def run_tac_test():
    def test_operator():
        #TAC.read_file('input1.txt')
        op = []
        op = TAC.get_operator('input1.txt')
        print op
        assert(op == True)

    def test_tac_format():
        op = TAC.get_tac_format('input1.txt')
        assert(op == True)

    def test_operand1():
        op = TAC.get_operand1('input1.txt')
        assert(op == True)

    def test_operand2():
        op = TAC.get_operand2('input1.txt')
        assert(op == True)

    def test_get_result():
        op = TAC.get_result('input1.txt')
        assert(op == True)


    test_operator()
    test_tac_format()
    test_operand1()
    test_get_operand2()
    test_get_result()
run_tac_test()
        

# -*- coding: UTF-8 -*-
import clipboard
import Image 

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)


    def test_copy_english_text():
        clipboard.reset()
        clipboard.copytext("hello, world!",15)
        text = clipboard.gettext()
        print text
        #assert(text == "hello, world!")

    def test_copy_blob():
        clipboard.reset()
        clipboard.copyblob(Image.Open("test.jpg"),160000)
        blob = clipboard.getblob()
        #cv2.imshow('image',blob)


    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        clipboard.copytext("hello, world!",10)
    
    test_one_observer()

run_clipboard_observer_tests()
